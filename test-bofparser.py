#!/usr/bin/python3

import unittest
import bofparser

def make_bof(name, time, time_end=None, room="42"):
    if time_end is None:
        return bofparser.BofSession(time=time, subject=name, description=name, room=room)
    else:
        return bofparser.BofSession(time=time, subject=name, description="From %s to %s - %s" % (time, time_end, name), room=room)

def make_bofs(tuples, room="42"):
    return [make_bof(*args, room=room) for args in tuples]

class TestBofMerge(unittest.TestCase):
    def assertBofMerge(self, input, output):
        return self.assertEqual(list(bofparser.merge_dups(make_bofs(input))), make_bofs(output))

    def test_empty(self):
        """Empty list"""
        self.assertBofMerge([], [])

    def test_single(self):
        """Only one item"""
        bofs = [
            ("A", "09:00"),
        ]
        self.assertBofMerge(bofs, bofs)

    def test_noop(self):
        """Nothing needs merging"""
        bofs = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00")
        ]
        self.assertBofMerge(bofs, bofs)

    def test_middle(self):
        """Group to merge is in the middle of the list, with several items before and after"""
        bofs = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00"),
            ("C", "12:00"),
            ("C", "13:00"),
            ("D", "14:00"),
            ("E", "15:00")
        ]
        result = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00", "14:00"),
            ("D", "14:00"),
            ("E", "15:00")
        ]
        self.assertBofMerge(bofs, result)

    def test_first(self):
        """Group to merge is at the beginning of the list."""
        bofs = [
            ("A", "09:00"),
            ("A", "10:00"),
            ("A", "11:00"),
            ("B", "12:00"),
            ("C", "13:00"),
        ]
        result = [
            ("A", "09:00", "12:00"),
            ("B", "12:00"),
            ("C", "13:00"),
        ]
        self.assertBofMerge(bofs, result)

    def test_next_last(self):
        """Group to merge is next to the end of the list, but there is one last element after it"""
        bofs = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00"),
            ("C", "12:00"),
            ("C", "13:00"),
            ("D", "14:00")
        ]
        result = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00", "14:00"),
            ("D", "14:00")
        ]
        self.assertBofMerge(bofs, result)

    def test_last(self):
        """Group to merge is at the end of the list."""
        bofs = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00"),
            ("C", "12:00"),
            ("C", "13:00"),
        ]
        result = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00"),
        ]
        self.assertBofMerge(bofs, result)

class TestPostproc(unittest.TestCase):
    """
    Tests the bof_postproc function
    """
    def test_basic(self):
        """Tests basic situation with a single room list"""
        # testcase from TestBofMerge.test_next_last
        bofs = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00"),
            ("C", "12:00"),
            ("C", "13:00"),
            ("D", "14:00")
        ]
        result = [
            ("A", "09:00"),
            ("B", "10:00"),
            ("C", "11:00", "14:00"),
            ("D", "14:00")
        ]
        self.assertEqual(list(bofparser.bof_postproc([make_bofs(bofs)])), make_bofs(result))

    def test_multi_room(self):
        """
        Tests a list with multiple BoF rooms, and where the first room has a
        multi-row BoF session. Ensures that the row group isn't merged with
        rows from the second room.
        """

        bofs_r1 = make_bofs([
            ("A", "09:00"),
            ("B", "10:00"),
            ("B", "11:00"),
            ("B", "12:00")
        ], room="1")
        bofs_r2 = make_bofs([
            ("B", "09:00"),
            ("B", "10:00"),
            ("C", "11:00"),
        ], room="2")
        result = [
            make_bof("A", "09:00", room="1"),
            make_bof("B", "10:00", room="1"),
            make_bof("B", "09:00", room="2", time_end="11:00"),
            make_bof("C", "11:00", room="2"),
        ]
        self.assertEqual(list(bofparser.bof_postproc([bofs_r1, bofs_r2])), result)

if __name__ == '__main__':
    unittest.main()
