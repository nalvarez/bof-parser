import logging

import attr
import requests
from bs4 import BeautifulSoup

logging.basicConfig(level=logging.INFO)

@attr.s
class BofSession:
    time = attr.ib()
    subject = attr.ib()
    description = attr.ib()
    room = attr.ib()


def parse_table(table):
    """
    Parses a wiki table containing the BoF schedule for a single room.
    Returns an iterable of BofSession objects. This is raw info, including
    empty rows.
    """
    assert 'wikitable' in table.attrs['class']

    rows = table.find_all('tr', recursive=False)
    # the first row has the room name
    room_name = rows[0].find('th').get_text().strip()

    # second row has headings
    cells = rows[1].find_all('th', recursive=False)
    assert [cell.get_text().strip() for cell in cells] == ['Time', 'Subject', 'Host/Notes']

    # following rows have actual BoFs
    for row in rows[2:]:
        cells = row.find_all('td', recursive=False)

        # We'll handle cell spans later, for now let's ignore them.
        # If there's a rowspan cell and it's not the last one in the table,
        # the parsing result will probably be wrong.

        yield BofSession(
            time = cells[0].get_text().strip().rjust(5,'0'),
            subject = cells[1].get_text().strip(),
            description = cells[2].get_text().strip() if len(cells) >=3 else '',
            room = room_name
        )

def parse_page(soup):
    """
    Parses the page and extracts every wiki table (there's one per room).
    Returns an iterable of iterables (one per table) of BofSession objects.
    """
    for table in soup.find_all("table"):
        yield parse_table(table)

def parse_page_html(html):
    return parse_page(BeautifulSoup(html, 'html.parser'))

def merge_dups(bofs):
    """
    If there are multiple consecutive BoFs with the same information, other
    than the time, merge them into a single one.

    All BoFs passed must have the same room, and the list must be sorted by time.
    This precondition is *not* checked.
    """
    logging.info("merge_dups called")

    bofs=iter(bofs)
    try:
        first = next(bofs)
    except StopIteration:
        # sequence was empty
        return

    logging.info("first item %s", first)
    prev = first
    group_first = first
    group_len = 1
    for bof in bofs:
        logging.info("processing %s", bof)

        # If any info other than the time differs, and we already have a group
        # of more than one equal item, create and yield the merged item. If the
        # current item differs but we don't have a group yet, it means the previous
        # item doesn't form a group; yield it unmodified.

        if (bof.room, bof.subject, bof.description) != (prev.room, prev.subject, prev.description):
            if group_len > 1:
                logging.info("didn't match; merging %d items from %s", group_len, group_first)
                yield attr.evolve(group_first,
                    description = "From %s to %s - %s" % (group_first.time, bof.time, group_first.description),
                )
            else:
                logging.info("didn't match, yielding prev")
                yield prev

            group_first = bof
            group_len = 0

        group_len += 1
        prev = bof

    # handle last item
    if group_len > 1:
        # last item is part of a group; yield the *first* item of the group,
        # since it has the correct start time
        yield group_first
    else:
        yield prev

def filter_empty(bofs):
    for bof in bofs:
        if bof.subject.strip() != '':
            yield bof

def bof_postproc(bofs):
    """
    Post-processes the raw list of BoFs.
    - Combines identical consecutive rows into a single BoF item
    - Removes empty rows
    - Flattens the nested list (see example)

    The input is a list of lists of BoFs (or rather an iterable of iterables),
    where each inner list has BoFs of the same room. The output is a single
    list (or iterable) of BoFs

    Example:
    bof_postproc([[room1_bof1, room1_bof2], [room2_bof1]]) == [room1_bof1, room1_bof2, room2_bof1]
    """

    result = []

    for room in bofs:
        result.extend(filter_empty(merge_dups(room)))

    return result

def make_bofs_json(bofs):
    return {'bof': [attr.asdict(bof) for bof in bofs]}

