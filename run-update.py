#!/usr/bin/env python3

import json

import bofparser

import requests

days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

try:
    with open("state.json", "r") as state_f:
        state = json.load(state_f)
except (OSError, ValueError):
    state = {}

for day in days:
    print("fetching %s" % day)
    resp = requests.get(
        'https://community.kde.org/api.php', params={
            'action': 'parse',
            'page': 'Akademy/2018/%s' % day,
            'prop': 'text|revid',
            'format': 'json'
        }
    )

    resp_json = resp.json()

    revid = resp_json['parse']['revid']
    if state.get(day, None) == revid:
        # the current file is already up-to-date
        print("skipping %s because we already have rev %d" % (day, revid))
        continue

    print("parsing %s and updating json" % day)

    html = resp_json['parse']['text']['*']

    bofs = bofparser.bof_postproc(bofparser.parse_page_html(html))
    with open("%s.json" % day.lower(), "w") as f:
        json.dump(bofparser.make_bofs_json(bofs), f, indent=1)

    print("updating state file")
    state[day] = revid
    with open("state.json", "w") as state_f:
        json.dump(state, state_f)
